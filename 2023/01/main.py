def part1(filename):
    s = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            first_digit = None
            last_digit = None
            for c in line:
                if c.isdigit():
                    if first_digit is None:
                        first_digit = int(c)
                    last_digit = int(c)
            s += first_digit * 10 + last_digit
    return s


def part2(filename):
    s = 0
    digits = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    with open(filename, 'r') as f:
        for line in f.readlines():
            first_digit = None
            last_digit = None
            for i in range(len(line)):
                if line[i].isdigit():
                    if first_digit is None:
                        first_digit = int(line[i])
                    last_digit = int(line[i])
                else:
                    for d in range(len(digits)):
                        if line[i:].startswith(digits[d]):
                            if first_digit is None:
                                first_digit = d+1
                            last_digit = d+1
                            i + len(digits[d])
                            break

            s += first_digit * 10 + last_digit
    return s


assert(part1("example.txt") == 142)
print(part1("input.txt"))

assert(part2("example2.txt") == 281)
print(part2("input.txt"))
