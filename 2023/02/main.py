def part1(filename):
    NB_CUBES = {
        "red": 12,
        "green": 13,
        "blue": 14,
    }

    answer = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            is_possible = True

            splitted_line = line.split(":")

            game_id = int(splitted_line[0].split(" ")[1])

            sets_line = splitted_line[1].split(";")
            for s in sets_line:
                cubes = s.split(",")
                for cube in cubes:
                    cube_split = cube.strip().split(" ")
                    if int(cube_split[0]) > NB_CUBES[cube_split[1]]:
                        is_possible = False
                        break
                if not is_possible:
                    break

            if is_possible:
                answer += game_id

    return answer


def part2(filename):
    answer = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            maximum_cubes = {
                "blue": 0,
                "red": 0,
                "green": 0,
            }
            sets = line.split(":")[1].split(";")
            for s in sets:
                cubes = s.split(",")
                for cube in cubes:
                    cube_split = cube.strip().split(" ")
                    color = cube_split[1]
                    number = int(cube_split[0])
                    if number > maximum_cubes[color]:
                        maximum_cubes[color] = number
            answer += maximum_cubes["blue"] * maximum_cubes["red"] * maximum_cubes["green"]

    return answer


assert(part1("example.txt") == 8)
print(part1("input.txt"))

assert(part2("example.txt") == 2286)
print(part2("input.txt"))
