def part1(filename):
    answer = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            numbers = line.split(": ")[1]
            numbers_split = numbers.split("|")
            winning_numbers = list(map(int, numbers_split[0].split()))
            own_numbers = list(map(int, numbers_split[1].split()))
            nb_winning_numbers = 0
            for own_number in own_numbers:
                if own_number in winning_numbers:
                    nb_winning_numbers += 1
            if nb_winning_numbers >= 1:
                answer += 1 << (nb_winning_numbers-1)

    return answer


def part2(filename):
    answer = 0
    with open(filename, 'r') as f:
        cards = []
        for line in f.readlines():
            numbers = line.split(": ")[1]
            numbers_split = numbers.split("|")
            winning_numbers = list(map(int, numbers_split[0].split()))
            own_numbers = list(map(int, numbers_split[1].split()))
            nb_winning_numbers = 0
            for own_number in own_numbers:
                if own_number in winning_numbers:
                    nb_winning_numbers += 1
            cards.append({
                "nb_winning_numbers": nb_winning_numbers,
                "nb_cards": 1,
            })

        for i in range(len(cards)):
            answer += cards[i]["nb_cards"]
            for j in range(i+1, min(len(cards), i+1+cards[i]["nb_winning_numbers"])):
                cards[j]["nb_cards"] += cards[i]["nb_cards"]

    return answer


assert(part1("example.txt") == 13)
print(part1("input.txt"))

assert(part2("example.txt") == 30)
print(part2("input.txt"))
