def part1(filename):
    def is_symbol(c):
        return c != '.' and not c.isdigit()

    answer = 0
    with open(filename, 'r') as f:
        matrix = f.readlines()
        for i in range(len(matrix)):
            line_length = len(matrix[i]) - 1
            current_number = 0
            next_to_symbol = False
            in_number = False

            for j in range(line_length):
                if matrix[i][j].isdigit():
                    current_number = current_number * 10 + int(matrix[i][j])
                    in_number = True
                    if not next_to_symbol:
                        if i > 0:
                            if j > 0 and is_symbol(matrix[i-1][j-1]):
                                next_to_symbol = True
                                continue
                            if is_symbol(matrix[i-1][j]):
                                next_to_symbol = True
                                continue
                            if j+1 < line_length and is_symbol(matrix[i-1][j+1]):
                                next_to_symbol = True
                                continue
                        if j > 0 and is_symbol(matrix[i][j-1]):
                            next_to_symbol = True
                            continue
                        if j+1 < line_length and is_symbol(matrix[i][j+1]):
                            next_to_symbol = True
                            continue
                        if i+1 < len(matrix):
                            if j > 0 and is_symbol(matrix[i+1][j-1]):
                                next_to_symbol = True
                                continue
                            if is_symbol(matrix[i+1][j]):
                                next_to_symbol = True
                                continue
                            if j+1 < line_length and is_symbol(matrix[i+1][j+1]):
                                next_to_symbol = True
                                continue
                else:
                    if in_number:  # we were in a number
                        if next_to_symbol:
                            answer += current_number
                        in_number = False
                        current_number = 0
                        next_to_symbol = False

            # The line ends with a number:
            if in_number:
                if next_to_symbol:
                    answer += current_number

    return answer


def part2(filename):
    answer = 0

    with open(filename, 'r') as f:
        matrix = f.readlines()

        nrows = len(matrix)
        ncols = len(matrix[0])-1

        matrix_numbers = [[None]*ncols for _ in range(nrows)]
        for i in range(nrows):
            j_number_start = None
            for j in range(ncols):
                if matrix[i][j].isdigit():
                    if j_number_start is None:
                        j_number_start = j
                        matrix_numbers[i][j] = 0
                    matrix_numbers[i][j_number_start] = matrix_numbers[i][j_number_start] * 10 + int(matrix[i][j])
                    for j_numbers in range(j_number_start+1, j+1):
                        matrix_numbers[i][j_numbers] = matrix_numbers[i][j_number_start]
                else:
                    j_number_start = None

        for i in range(nrows):
            for j in range(ncols):
                if matrix[i][j] == '*':
                    adjacent_numbers = []
                    if i > 0:
                        if j > 0 and matrix_numbers[i-1][j-1] is not None:
                            adjacent_numbers.append(matrix_numbers[i-1][j-1])
                        if matrix_numbers[i-1][j] is not None:
                            adjacent_numbers.append(matrix_numbers[i-1][j])
                        if j+1 < ncols and matrix_numbers[i-1][j+1] is not None:
                            adjacent_numbers.append(matrix_numbers[i-1][j+1])
                    if j > 0 and matrix_numbers[i][j-1] is not None:
                        adjacent_numbers.append(matrix_numbers[i][j-1])
                    if j+1 < ncols and matrix_numbers[i][j+1]:
                        adjacent_numbers.append(matrix_numbers[i][j+1])
                    if i+1 < nrows:
                        if j > 0 and matrix_numbers[i+1][j-1]:
                            adjacent_numbers.append(matrix_numbers[i+1][j-1])
                        if matrix_numbers[i+1][j]:
                            adjacent_numbers.append(matrix_numbers[i+1][j])
                        if j+1 < ncols and matrix_numbers[i+1][j+1]:
                            adjacent_numbers.append(matrix_numbers[i+1][j+1])

                    # Using sets to make each value unique in the list might not work in all cases
                    if len(set(adjacent_numbers)) == 2:
                        answer += list(set(adjacent_numbers))[0] * list(set(adjacent_numbers))[1]

    return answer


assert(part1("example.txt") == 4361)
print(part1("input.txt"))

assert(part2("example.txt") == 467835)
print(part2("input.txt"))
