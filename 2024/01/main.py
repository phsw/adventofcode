def part1(filename):
    answer = 0
    with open(filename, 'r') as f:
        left = []
        right = []

        for line in f.readlines():
            splitted = list(map(int, line.split()))
            left.append(splitted[0])
            right.append(splitted[1])

        left.sort()
        right.sort()

        for i in range(len(left)):
            answer += abs(left[i] - right[i])

    return answer

def part2(filename):
    answer = 0
    with open(filename, 'r') as f:
        left = []
        right = []

        for line in f.readlines():
            splitted = list(map(int, line.split()))
            left.append(splitted[0])
            right.append(splitted[1])

        for e in left:
            nb = 0
            for j in range(len(right)):
                if e == right[j]:
                    nb += 1

            answer += e * nb

    return answer

assert(part1("example.txt") == 11)
print(part1("input.txt"))

assert(part2("example.txt") == 31)
print(part2("input.txt"))
