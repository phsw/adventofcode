import re


def part1(filename):
    answer = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            m = re.findall(r"(mul\(\d{1,3},\d{1,3}\))", line)
            for e in m:
                sub = list(map(int, e[4:-1].split(",")))
                answer += sub[0]*sub[1]

    return answer


def part2(filename):
    def parse(s):
        a = 0
        m = re.findall(r"(mul\(\d{1,3},\d{1,3}\))", line)
        for e in m:
            sub = list(map(int, e[4:-1].split(",")))
            a += sub[0]*sub[1]
        return a

    answer = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            i_to = line.find("don't()")
            if i_to == -1:
                answer += parse(line)
            else:
                answer += parse(line[:i_to])
                line = line[i_to:]



    return answer

assert(part1("example.txt") == 161)
print(part1("input.txt"))

assert(part2("example2.txt") == 48)
print(part1("input.txt"))
