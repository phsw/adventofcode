def part1(filename):
    answer = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            numbers = list(map(int, line.split()))
            is_valid = True

            if numbers[1] > numbers[0]:
                # increasing
                for i in range(len(numbers)-1):
                    diff = numbers[i+1] - numbers[i]
                    if not 1 <= diff < 4:
                        is_valid = False
                        break
            else:
                # decreasing
                for i in range(len(numbers)-1):
                    diff = numbers[i] - numbers[i+1]
                    if not 1 <= diff < 4:
                        is_valid = False
                        break

            if is_valid:
                answer += 1

    return answer

def part2(filename):
    def is_safe(report, exclude=None):
        is_valid = True

        report_to_test = []
        if exclude is not None:
            for i in range(len(report)):
                if i != exclude :
                    report_to_test.append(report[i])
        else:
            report_to_test = report

        if report_to_test[1] > report_to_test[0]:
            # increasing
            for i in range(len(report_to_test)-1):
                diff = report_to_test[i+1] - report_to_test[i]
                if not 1 <= diff < 4:
                    is_valid = False
                    break
        else:
            # decreasing
            for i in range(len(report_to_test)-1):
                diff = report_to_test[i] - report_to_test[i+1]
                if not 1 <= diff < 4:
                    is_valid = False
                    break

        return is_valid


    answer = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            numbers = list(map(int, line.split()))
            if is_safe(numbers):
                answer += 1
            else:
                for i in range(len(numbers)):
                    if is_safe(numbers, i):
                        answer += 1
                        break

    return answer

assert(part1("example.txt") == 2)
print(part1("input.txt"))

assert(part2("example.txt") == 4)
print(part2("input.txt"))
