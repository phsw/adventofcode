def sum_unmarked(grid, marked_grid, dim):
    s = 0
    for i in range(dim):
        for j in range(dim):
            if not marked_grid[i][j]:
                s += grid[i][j]

    return s


def part1(grids, nb_grids, dim, drawn_numbers):
    marked_grids = [[[False for _ in range(dim)] for _ in range(dim)] for _ in range(nb_grids)]
    winning_grid = None
    last_drawn_number = None

    for n in drawn_numbers:
        for g in range(nb_grids):
            for i in range(dim):
                for j in range(dim):
                    if grids[g][i][j] == n:
                        marked_grids[g][i][j] = True

            for i in range(dim):
                if all(marked_grids[g][i]) or all([marked_grids[g][j][i] for j in range(dim)]):
                    winning_grid = g
                    last_drawn_number = n
                    break

            if winning_grid is not None:
                break

        if winning_grid is not None:
            break

    assert(winning_grid is not None)
    return winning_grid, last_drawn_number, marked_grids[winning_grid]


def part2(grids, nb_grids, dim, drawn_numbers):
    marked_grids = [[[False for _ in range(dim)] for _ in range(dim)] for _ in range(nb_grids)]
    won_grids = []
    last_drawn_number = None

    for n in drawn_numbers:
        for g in range(nb_grids):
            if g not in won_grids:
                for i in range(dim):
                    for j in range(dim):
                        if grids[g][i][j] == n:
                            marked_grids[g][i][j] = True

                for i in range(dim):
                    if all(marked_grids[g][i]) or all([marked_grids[g][j][i] for j in range(dim)]):
                        won_grids.append(g)
                        last_drawn_number = n
                        break

                if len(won_grids) == nb_grids:
                    break

        if len(won_grids) == nb_grids:
            break

    return won_grids[-1], last_drawn_number, marked_grids[won_grids[-1]]


def problem(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
        drawn_numbers = list(map(int, content[0].split(",")))
        grids = []

        for l in content[1:]:
            if len(l.strip()) == 0:
                grids.append([])
            else:
                grids[-1].append(list(map(int, l.split())))

        nb_grids = len(grids)
        dim  = len(grids[0])
        assert(len(grids[0][0]) == dim)

        winning_grid_id, last_drawn_number, marked_grid = part1(grids, nb_grids, dim, drawn_numbers)
        answer1 = sum_unmarked(grids[winning_grid_id], marked_grid, dim) * last_drawn_number

        last_winning_grid_id, last_drawn_number, marked_grid = part2(grids, nb_grids, dim, drawn_numbers)
        answer2 = sum_unmarked(grids[last_winning_grid_id], marked_grid, dim) * last_drawn_number

        return answer1, answer2


assert(problem("test.txt") == (4512, 1924))
print(problem("input.txt"))
