def binary2int(b):
    v = 0
    for i in range(len(b)):
        v += 2**i * int(b[len(b)-i-1])
    return v


def get_hist(numbers, digit):
    hist_values = {
        "0": 0,
        "1": 0
    }

    for n in numbers:
        hist_values[n[digit]] += 1

    return hist_values


def problem(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
        nb_digits = len(content[0].strip())

        # Part 1:
        gamma_binary = ""
        epsilon_binary = ""
        for i in range(nb_digits):
            hist_values = get_hist(content, i)

            if hist_values["0"] > hist_values["1"]:
                gamma_binary += "0"
                epsilon_binary += "1"
            else:
                gamma_binary += "1"
                epsilon_binary += "0"

        # Part 2:
        ox_numbers = [s.strip() for s in content]
        co_numbers = [s.strip() for s in content]
        for i in range(nb_digits):
            if len(ox_numbers) > 1:
                hist_ox = get_hist(ox_numbers, i)
                max_value = "1"
                if hist_ox["0"] > hist_ox["1"]:
                    max_value = "0"
                ox_numbers = [n for n in ox_numbers if n[i] == max_value]

            if len(co_numbers) > 1:
                hist_co = get_hist(co_numbers, i)
                max_value = "0"
                if hist_co["0"] > hist_co["1"]:
                    max_value = "1"
                co_numbers = [n for n in co_numbers if n[i] == max_value]

        return binary2int(gamma_binary) * binary2int(epsilon_binary), binary2int(co_numbers[0]) * binary2int(ox_numbers[0])

assert(binary2int("10110") == 22)
print(problem("test.txt"))
assert(problem("test.txt") == (198, 230))

print(problem("input.txt"))
