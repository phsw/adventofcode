with open("input.txt", 'r') as f:
    content = f.readlines()

    h = 0
    d = 0
    for line in content:
        v = line.strip().split()

        if v[0] == "forward":
            h += int(v[1])
        elif v[0] == "down":
            d += int(v[1])
        elif v[0] == "up":
            d -= int(v[1])
        else:
            raise Exception("Unknown direction")

    print(d*h)

    aim = 0
    d = 0
    h = 0

    for line in content:
        v = line.strip().split()

        if v[0] == "forward":
            h += int(v[1])
            d += aim * int(v[1])
        elif v[0] == "down":
            aim += int(v[1])
        elif v[0] == "up":
            aim -= int(v[1])
        else:
            raise Exception("Unknown direction")

    print(d*h)
