import statistics


def problem(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
        positions = list(map(int, content[0].split(",")))
        med = statistics.median(positions)

        fuel1 = 0
        for p in positions:
            fuel1 += abs(p - med)

        # Brute-force for part 2:
        min_position = min(positions)
        max_position = max(positions)
        fuel2 = None
        for i in range(min_position, max_position+1):
            tmp_fuel = 0
            for p in positions:
                n = abs(p - i)
                tmp_fuel += n*(n+1)/2
            if fuel2 is None:
                fuel2 = tmp_fuel
            else:
                fuel2 = min(fuel2, tmp_fuel)

        return fuel1, fuel2


assert(problem("test.txt") == (37, 168))
print(problem("input.txt"))
