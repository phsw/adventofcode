with open("input.txt", 'r') as f:
    content = f.readlines()
    numbers = [int(c) for c in content]

    c = 0
    for i in range(1, len(numbers)):
        if numbers[i] > numbers[i-1]:
            c += 1
    print(c)

    c = 0
    for i in range(4, len(numbers)+1):
        previous = numbers[i-4:i-1]
        current = numbers[i-3:i]
        if sum(current) > sum(previous):
            c += 1
    print(c)
