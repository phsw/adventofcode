def get_nb_overlaps(grid, width, height):
    nb_overlaps = 0
    for x in range(width):
        for y in range(height):
            if grid[y][x] > 1:
                nb_overlaps += 1
    return nb_overlaps


def my_range(start, end):
    if start < end:
        return list(range(start, end+1))
    elif start > end:
        return list(range(start, end-1, -1))
    else:
        raise Exception("Shouldn't happen")


def problem(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
        data = []
        width = 0
        height = 0

        for line in content:
            coords = line.strip().split(" -> ")
            begin = list(map(int, coords[0].split(",")))
            end = list(map(int, coords[1].split(",")))

            width = max(width, begin[0], end[0])
            height = max(height, begin[1], end[1])

            data.append({
                "begin": {
                    "x": begin[0],
                    "y": begin[1]
                },
                "end": {
                    "x": end[0],
                    "y": end[1]
                }
            })

        height += 1
        width += 1

        grid1 = [[0 for _ in range(width)] for _ in range(height)]
        grid2 = [[0 for _ in range(width)] for _ in range(height)]
        for d in data:
            real_begin_x = min(d["begin"]["x"], d["end"]["x"])
            real_end_x = max(d["begin"]["x"], d["end"]["x"])
            real_begin_y = min(d["begin"]["y"], d["end"]["y"])
            real_end_y = max(d["begin"]["y"], d["end"]["y"])

            # Part 1 & Part 2:
            if real_begin_x == real_end_x or real_begin_y == real_end_y:
                for x in range(real_begin_x, real_end_x+1):
                    for y in range(real_begin_y, real_end_y+1):
                        grid1[y][x] += 1
                        grid2[y][x] += 1
            else:
                # Part 2:
                list_x = my_range(d["begin"]["x"], d["end"]["x"])
                assert(len(list_x) > 1)
                list_y = my_range(d["begin"]["y"], d["end"]["y"])
                assert(len(list_y) == len(list_x))

                for i in range(len(list_x)):
                    grid2[list_y[i]][list_x[i]] += 1

        return get_nb_overlaps(grid1, width, height), get_nb_overlaps(grid2, width, height)


assert(problem("test.txt") == (5, 12))
print(problem("input.txt"))
