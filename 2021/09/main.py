def problem(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
        heightmap = [[int(c) for c in line.strip()] for line in content]
        height = len(heightmap)
        width = len(heightmap[0])

        answer1 = 0
        for i in range(height):
            for j in range(width):
                center = heightmap[i][j]

                if i > 0 and center >= heightmap[i-1][j]:
                    continue
                if i < (height-1) and center >= heightmap[i+1][j]:
                    continue
                if j > 0 and center >= heightmap[i][j-1]:
                    continue
                if j < (width-1) and center >= heightmap[i][j+1]:
                    continue
                answer1 += center + 1

        return answer1, None


assert(problem("test.txt") == (15, None))
print(problem("input.txt"))
