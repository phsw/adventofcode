# Naive version, too slow:
def problem(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
        l = list(map(int, content[0].split(",")))

        for i in range(80):
            nb = len(l)
            for j in range(nb):
                if l[j] == 0:
                    l[j] = 6
                    l.append(8)
                else:
                    l[j] -= 1

        answer1 = len(l)

        return answer1, None


def problem2(filename):
    def step(hist):
        new_hist = { i: 0 for i in range(9) }
        new_hist[6] = hist[0]
        new_hist[8] = hist[0]
        for i in range(8):
            new_hist[i] += hist[i+1]
        return new_hist

    with open(filename, 'r') as f:
        content = f.readlines()
        l = list(map(int, content[0].split(",")))

        hist = { i: 0 for i in range(9) }
        for e in l:
            hist[e] += 1

        for _ in range(80):
            hist = step(hist)

        answer1 = sum(hist.values())

        for _ in range(256-80):
            hist = step(hist)

        return answer1, sum(hist.values())


assert(problem2("test.txt") == (5934, 26984457539))
print(problem2("input.txt"))
