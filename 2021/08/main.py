def problem(filename):
    with open(filename, 'r') as f:
        content = f.readlines()
        data = []
        for l in content:
            [left, right] = l.split(" | ")
            data.append({
                "left": left.split(),
                "right": right.split()
            })

        answer1 = 0
        for d in data:
            for w in d["right"]:
                if len(w) in [2, 4, 3, 7]:
                    answer1 += 1

        answer2 = 0
        for d in data:
            placement = {
                "up": None,
                "middle": None,
                "bottom": None,
                "left-up": None,
                "right-up": None,
                "left-bottom": None,
                "right-bottom": None
            }
            words = d["left"] + d["right"]
            for i in range(len(words)):
                if len(words[i]) == 2:  # one
                    for j in range(len(words)):
                        if j != i:
                            if len(words[j]) == 3:  # seven
                                for letter in words[j]:
                                    if letter not in words[i]:



        return answer1, answer2


assert(problem("test.txt") == (26, 61229))
print(problem("input.txt"))
