def convert_str_to_path(s):
    p = []
    i = 0
    while i < len(s):
        if s[i] == "e" or s[i] == "w":
            p.append(s[i])
            i += 1
        else:
            p.append(s[i:i+2])
            i += 2
    return p


def get_coord(path):
    x = 0
    y = 0

    for p in path:
        if p == "e":
            x += 2
        elif p == "se":
            x += 1
            y -= 1
        elif p == "sw":
            x -= 1
            y -= 1
        elif p == "w":
            x -= 2
        elif p == "nw":
            x -= 1
            y += 1
        elif p == "ne":
            x += 1
            y += 1
        else:
            raise Exception("unknown direction")

    # print(path, "-> (", x, ",", y, ")")
    return x, y


def add_missing_tiles(tiles):
    min_x = 0
    max_x = 0
    min_y = 0
    max_y = 0

    for x, y in tiles:
        min_x = min(min_x, x)
        max_x = max(max_x, x)
        min_y = min(min_y, y)
        max_y = max(max_y, y)

    min_x -= 2
    max_x += 2
    min_y -= 2
    max_y += 2

    for x in range(min_x, max_x+1):
        for y in range(min_y, max_y+1):
            if x % 2 == 0:
                if y % 2 == 0 and (x, y) not in tiles:
                    tiles[(x, y)] = "white"
            else:
                if y % 2 == 1 and (x, y) not in tiles:
                    tiles[(x, y)] = "white"


def count_black_neighbors(tiles, x, y):
    nb = 0
    if (x-2, y) in tiles and tiles[(x-2, y)] == "black":
        nb += 1
    if (x-1, y+1) in tiles and tiles[(x-1, y+1)] == "black":
        nb += 1
    if (x+1, y+1) in tiles and tiles[(x+1, y+1)] == "black":
        nb += 1
    if (x+2, y) in tiles and tiles[(x+2, y)] == "black":
        nb += 1
    if (x+1, y-1) in tiles and tiles[(x+1, y-1)] == "black":
        nb += 1
    if (x-1, y-1) in tiles and tiles[(x-1, y-1)] == "black":
        nb += 1
    return nb


def get_new_tiles(tiles):
    new_tiles = dict()

    for x, y in tiles:
        assert((x, y) not in new_tiles)
        nb = count_black_neighbors(tiles, x, y)
        if tiles[(x,y)] == "black":
            if nb == 0 or nb > 2:
                new_tiles[(x, y)] = "white"
            else:
                new_tiles[(x, y)] = "black"
        else:
            if nb == 2:
                new_tiles[(x, y)] = "black"
            else:
                new_tiles[(x, y)] = "white"
        # if tiles[(x, y)] != new_tiles[(x, y)]:
            # print("(", x, ",", y, ") was", tiles[(x, y)], "with", nb, "neighbors, so new tile is", new_tiles[(x, y)])

    return new_tiles


def count_black(tiles):
    nb = 0
    for e in tiles:
        if tiles[e] == "black":
            nb += 1
    return nb


def print_black_coord(tiles):
    print("** black tiles: **")
    for e in tiles:
        if tiles[e] == "black":
            print(e)


assert(get_coord(convert_str_to_path("nwwswee")) == (0,0))
assert(get_coord(convert_str_to_path("esew")) == (1,-1))

with open("input.txt", 'r') as f:
    content = f.readlines()
    lines = []

    for l in content:
        lines.append(convert_str_to_path(l.strip()))

    tiles = dict()
    nb_black = 0
    for line in lines:
        x, y = get_coord(line)
        if (x, y) not in tiles:
            tiles[(x, y)] = "white"

        if tiles[(x, y)] == "white":
            tiles[(x, y)] = "black"
            nb_black += 1
        else:
            tiles[(x, y)] = "white"
            nb_black -= 1

    print(nb_black)
    # print(tiles)
    # print(count_black(tiles))
    # print_black_coord(tiles)

    # part 2:
    for i in range(100):
        add_missing_tiles(tiles)
        # print(tiles)
        tiles = get_new_tiles(tiles)
        nb = count_black(tiles)
        print(i, nb)
        # print_black_coord(tiles)





