from enum import Enum


"""
Versions denote possible transformations of an image: rotations and/or flipping
Transformations making different images are:
- no transformation
- horizontal flip
- horizontal and vertical flip
- vertical flip
- rotation of 90°
- rotation of 90° then horizontal flip
- rotation of 90° then horizontal and vertical flip
- rotation of 90° then vertical flip
Other transformations (greater rotations) lead to already obtained images.
"""
NB_VERSIONS = 9

VERBOSE = False


class Direction(Enum):
    TOP = 0
    BOTTOM = 1
    LEFT = 2
    RIGHT = 3


def get_border(m, version, direction):
    if (version == 1 and direction == Direction.TOP) or (version == 2 and direction == Direction.BOTTOM) or (version == 8 and direction == Direction.LEFT) or (version == 5 and direction == Direction.RIGHT):
        return list(m[0])
    elif (version == 1 and direction == Direction.BOTTOM) or (version == 2 and direction == Direction.TOP) or (version == 8 and direction == Direction.RIGHT) or (version == 5 and direction == Direction.LEFT):
        return list(m[-1])
    elif (version == 1 and direction == Direction.LEFT) or (version == 4 and direction == Direction.RIGHT) or (version == 7 and direction == Direction.BOTTOM) or (version == 8 and direction == Direction.TOP):
        return [row[0] for row in m]
    elif (version == 1 and direction == Direction.RIGHT) or (version == 4 and direction == Direction.LEFT) or (version == 7 and direction == Direction.TOP) or (version == 8 and direction == Direction.BOTTOM):
        return [row[-1] for row in m]
    elif (version == 2 and direction == Direction.LEFT) or (version == 3 and direction == Direction.RIGHT) or (version == 5 and direction == Direction.TOP) or (version == 6 and direction == Direction.BOTTOM):
        return list(reversed([row[0] for row in m]))
    elif (version == 2 and direction == Direction.RIGHT) or (version == 3 and direction == Direction.LEFT) or (version == 5 and direction == Direction.BOTTOM) or (version == 6 and direction == Direction.TOP):
        return list(reversed([row[-1] for row in m]))
    elif (version == 3 and direction == Direction.TOP) or (version == 4 and direction == Direction.BOTTOM) or (version == 6 and direction == Direction.LEFT) or (version == 7 and direction == Direction.RIGHT):
        return list(reversed(m[-1]))
    elif (version == 3 and direction == Direction.BOTTOM) or (version == 4 and direction == Direction.TOP) or (version == 6 and direction == Direction.RIGHT) or (version == 7 and direction == Direction.LEFT):
        return list(reversed(m[0]))
    else:
        raise Exception("wrong version / direction")


def check_possibility(matrix, tiles, tiles_id, tile_id, version, line_id, col_id):
    """
    We only need to check compatibility with the upper tile and left tile,
    because there is no tile at the right side or at the bottom (due to the
    backtracking algorithm).
    """
    if VERBOSE:
        print("Checking if tile id", tile_id, "(", tiles_id[tile_id], ") version", version, "can be in ", line_id, col_id)
    if line_id > 0 and get_border(tiles[tiles_id[matrix[line_id-1][col_id]['tile_id']]], matrix[line_id-1][col_id]['version'], Direction.BOTTOM) != get_border(tiles[tiles_id[tile_id]], version, Direction.TOP):
        return False
    if col_id > 0 and get_border(tiles[tiles_id[matrix[line_id][col_id-1]['tile_id']]], matrix[line_id][col_id-1]['version'], Direction.RIGHT) != get_border(tiles[tiles_id[tile_id]], version, Direction.LEFT):
        return False

    return True


def next_tile(tile_id, version, taken_tiles):
    version += 1
    if version == NB_VERSIONS:
        version = 1
        tile_id += 1
        while tile_id < len(taken_tiles) and taken_tiles[tile_id]:
            tile_id += 1

    return tile_id, version


def get_value(m, version, line_id, col_id):
    if version == 1:
        return m[line_id][col_id]
    elif version == 2:
        return m[len(m)-1-line_id][col_id]
    elif version == 3:
        return m[len(m)-1-line_id][len(m)-1-col_id]
    elif version == 4:
        return m[line_id][len(m)-1-col_id]
    elif version == 5:
        return m[len(m)-1-col_id][line_id]
    elif version == 6:
        return m[len(m)-1-col_id][len(m)-1-line_id]
    elif version == 7:
        return m[col_id][len(m)-1-line_id]
    elif version == 8:
        return m[col_id][line_id]
    else:
        raise Exception("wrong version")


def count_monsters(image, version):
    monster = [
        [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, '#', None],
        ['#', None, None, None, None, '#', '#', None, None, None, None, '#', '#', None, None, None, None, '#', '#', '#'],
        [None, '#', None, None, '#', None, None, '#', None, None, '#', None, None, '#', None, None, '#', None, None, None]
    ]

    nb_monsters = 0

    for line_id in range(len(image)+2-len(monster)):
        for col_id in range(len(image[0])+2-len(monster[0])):
            is_monster = True
            for i in range(len(monster)):
                for j in range(len(monster[0])):
                    if monster[i][j] is not None and get_value(image, version, line_id+i, col_id+j) != monster[i][j]:
                        is_monster = False
                        break
                if not is_monster:
                    break
            if is_monster:
                nb_monsters += 1

    return nb_monsters


def count_out_monster(image):
    nb = 0
    for line in image:
        for e in line:
            if e == "#":
                nb += 1

    return nb



test_matrix = [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h', 'i']]
assert(get_border(test_matrix, 1, Direction.TOP) == ['a', 'b', 'c'])
assert(get_border(test_matrix, 2, Direction.TOP) == ['g', 'h', 'i'])
assert(get_border(test_matrix, 3, Direction.TOP) == ['i', 'h', 'g'])
assert(get_border(test_matrix, 4, Direction.TOP) == ['c', 'b', 'a'])
assert(get_border(test_matrix, 5, Direction.TOP) == ['g', 'd', 'a'])
assert(get_border(test_matrix, 6, Direction.TOP) == ['i', 'f', 'c'])
assert(get_border(test_matrix, 7, Direction.TOP) == ['c', 'f', 'i'])
assert(get_border(test_matrix, 8, Direction.TOP) == ['a', 'd', 'g'])
assert(get_border(test_matrix, 1, Direction.BOTTOM) == ['g', 'h', 'i'])
assert(get_border(test_matrix, 2, Direction.BOTTOM) == ['a', 'b', 'c'])
assert(get_border(test_matrix, 3, Direction.BOTTOM) == ['c', 'b', 'a'])
assert(get_border(test_matrix, 4, Direction.BOTTOM) == ['i', 'h', 'g'])
assert(get_border(test_matrix, 5, Direction.BOTTOM) == ['i', 'f', 'c'])
assert(get_border(test_matrix, 6, Direction.BOTTOM) == ['g', 'd', 'a'])
assert(get_border(test_matrix, 7, Direction.BOTTOM) == ['a', 'd', 'g'])
assert(get_border(test_matrix, 8, Direction.BOTTOM) == ['c', 'f', 'i'])
assert(get_border(test_matrix, 1, Direction.LEFT) == ['a', 'd', 'g'])
assert(get_border(test_matrix, 2, Direction.LEFT) == ['g', 'd', 'a'])
assert(get_border(test_matrix, 3, Direction.LEFT) == ['i', 'f', 'c'])
assert(get_border(test_matrix, 4, Direction.LEFT) == ['c', 'f', 'i'])
assert(get_border(test_matrix, 5, Direction.LEFT) == ['g', 'h', 'i'])
assert(get_border(test_matrix, 6, Direction.LEFT) == ['i', 'h', 'g'])
assert(get_border(test_matrix, 7, Direction.LEFT) == ['c', 'b', 'a'])
assert(get_border(test_matrix, 8, Direction.LEFT) == ['a', 'b', 'c'])
assert(get_border(test_matrix, 1, Direction.RIGHT) == ['c', 'f', 'i'])
assert(get_border(test_matrix, 2, Direction.RIGHT) == ['i', 'f', 'c'])
assert(get_border(test_matrix, 3, Direction.RIGHT) == ['g', 'd', 'a'])
assert(get_border(test_matrix, 4, Direction.RIGHT) == ['a', 'd', 'g'])
assert(get_border(test_matrix, 5, Direction.RIGHT) == ['a', 'b', 'c'])
assert(get_border(test_matrix, 6, Direction.RIGHT) == ['c', 'b', 'a'])
assert(get_border(test_matrix, 7, Direction.RIGHT) == ['i', 'h', 'g'])
assert(get_border(test_matrix, 8, Direction.RIGHT) == ['g', 'h', 'i'])



with open("input.txt", 'r') as f:
    content = f.readlines()

    tiles = dict()
    taken_tiles = []
    current_tile = None
    tiles_id = []

    for l in content:
        if len(l.strip()) == 0:
            current_tile = None
        elif current_tile is None:
            current_tile = int(l.strip()[5:-1])
            tiles[current_tile] = []
            taken_tiles.append(False)
            tiles_id.append(current_tile)
        else:
            tiles[current_tile].append(l.strip())

    dim = int(len(tiles) ** 0.5)
    print(len(tiles))
    print(dim)

    final_matrix = [[None for _ in range(dim)] for _ in range(dim)]

    # Backtracking algorithm:
    line_id = 0
    while line_id < dim:
        col_id = 0
        while col_id < dim:
            if VERBOSE:
                print(line_id, col_id)
                print(taken_tiles)
            assert(line_id >= 0)
            assert(line_id < dim)
            assert(col_id >= 0)
            assert(col_id < dim)

            tile_id = 0
            version = 1
            if final_matrix[line_id][col_id] is not None:
                tile_id, version = next_tile(final_matrix[line_id][col_id]['tile_id'], final_matrix[line_id][col_id]['version'], taken_tiles)
            else:
                while tile_id < len(tiles) and taken_tiles[tile_id]:
                    tile_id += 1
            if VERBOSE:
                print("Will try tile_id", tile_id, "version", version)

            while tile_id < len(tiles) and check_possibility(final_matrix, tiles, tiles_id, tile_id, version, line_id, col_id) is False:
                tile_id, version = next_tile(tile_id, version, taken_tiles)

            if tile_id < len(tiles):
                if final_matrix[line_id][col_id] is not None:
                    taken_tiles[final_matrix[line_id][col_id]['tile_id']] = False

                final_matrix[line_id][col_id] = {
                    "tile_id": tile_id,
                    "version": version
                }
                taken_tiles[tile_id] = True
                col_id += 1
            else:
                if final_matrix[line_id][col_id] is not None:
                    taken_tiles[final_matrix[line_id][col_id]['tile_id']] = False
                final_matrix[line_id][col_id] = None

                if col_id > 0:
                    col_id -= 1
                else:
                    line_id -= 1
                    col_id = dim-1

        line_id += 1

    print(tiles_id[final_matrix[0][0]['tile_id']] * tiles_id[final_matrix[0][-1]['tile_id']] * tiles_id[final_matrix[-1][0]['tile_id']] * tiles_id[final_matrix[-1][-1]['tile_id']])

    # part 2:
    tile_dim = len(tiles[tiles_id[0]])
    full_image = []
    for line_id in range(dim):
        for col_id in range(dim):
            for i in range(1, tile_dim-1):
                if col_id == 0:
                    full_image.append([])
                for j in range(1, tile_dim-1):
                    full_image[(line_id*(tile_dim-2))+i-1].append(
                        get_value(tiles[tiles_id[final_matrix[line_id][col_id]['tile_id']]], final_matrix[line_id][col_id]['version'], i, j)
                    )

    for line in full_image:
        print("".join(line))

    for v in range(1, NB_VERSIONS):
        nb_monsters = count_monsters(full_image, v)
        nb_out = count_out_monster(full_image)
        print("Version", v, nb_monsters, "monsters (", nb_out - (nb_monsters*15))
