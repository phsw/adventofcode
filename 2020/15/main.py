def play(turns, numbers):
    turn_id = 0
    apparitions = dict()
    previous_apparitions = dict()

    for n in numbers:
        turn_id += 1
        assert(n not in apparitions)
        apparitions[n] = [turn_id]

    said = numbers[-1]
    spoken = None
    while turn_id < turns:
        turn_id += 1
        # print("** Turn " + str(turn_id))
        if said in apparitions:
            if len(apparitions[said]) < 2:
                spoken = 0
            else:
                spoken = apparitions[said][-1] - apparitions[said][-2]
        else:
            spoken = 0

        if spoken not in apparitions:
            apparitions[spoken] = [turn_id]
        elif len(apparitions[spoken]) == 2:
            apparitions[spoken][0] = apparitions[spoken][1]
            apparitions[spoken][1] = turn_id
        else:
            apparitions[spoken].append(turn_id)

        said = spoken
        # print("I say " + str(spoken))

    return spoken


assert(play(2020, [0,3,6]) == 436)
assert(play(2020, [1,3,2]) == 1)
assert(play(2020, [2,1,3]) == 10)
assert(play(2020, [1,2,3]) == 27)
assert(play(2020, [2,3,1]) == 78)
assert(play(2020, [3,2,1]) == 438)
assert(play(2020, [3,1,2]) == 1836)
print(play(2020, [12,20,0,6,1,17,7]))

assert(play(30000000, [0,3,6]) == 175594)
assert(play(30000000, [1,3,2]) == 2578)
assert(play(30000000, [2,1,3]) == 3544142)
assert(play(30000000, [1,2,3]) == 261214)
assert(play(30000000, [2,3,1]) == 6895259)
assert(play(30000000, [3,2,1]) == 18)
assert(play(30000000, [3,1,2]) == 362)
print(play(30000000, [12,20,0,6,1,17,7]))


