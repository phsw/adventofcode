
with open("input.txt", 'r') as f:
    content = f.readlines()
    instructions = [[l[0], int(l[1:].strip())] for l in content]
    north = 0
    east = 0
    facing = 90

    for inst in instructions:
        if inst[0] == "N":
            north += inst[1]
        elif inst[0] == "S":
            north -= inst[1]
        elif inst[0] == "E":
            east += inst[1]
        elif inst[0] == "W":
            east -= inst[1]
        elif inst[0] == "L":
            facing = ((facing + 360) - inst[1]) % 360
        elif inst[0] == "R":
            facing = (facing + inst[1]) % 360
        elif inst[0] == "F":
            if facing == 0:
                north += inst[1]
            elif facing == 90:
                east += inst[1]
            elif facing == 180:
                north -= inst[1]
            elif facing == 270:
                east -= inst[1]

        assert(facing in [0, 90, 180, 270])

    print(north, east, abs(north)+abs(east))

    # part 2:
    north = 0
    east = 0
    w_north = 1
    w_east = 10

    for inst in instructions:
        if inst[0] == "N":
            w_north += inst[1]
        elif inst[0] == "S":
            w_north -= inst[1]
        elif inst[0] == "E":
            w_east += inst[1]
        elif inst[0] == "W":
            w_east -= inst[1]
        elif inst[0] == "L":
            deg = 0
            while deg < inst[1]:
                old_w_east = w_east
                w_east = -w_north
                w_north = old_w_east
                deg += 90
        elif inst[0] == "R":
            deg = 0
            while deg < inst[1]:
                old_w_east = w_east
                w_east = w_north
                w_north = -old_w_east
                deg += 90
        elif inst[0] == "F":
            north += w_north*inst[1]
            east += w_east*inst[1]

    print(north, east, abs(north)+abs(east))

