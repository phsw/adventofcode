import sys


def is_valid(bus_ids, bus_delay, t):
    for i in range(len(bus_ids)):
        if (t+bus_delay[i]) % bus_ids[i] != 0:
            return False

    return True


"""
Returns the first x > 0 such as ax+by=c when a and b are prime together
"""
def dioph(a, b, c):
    t = 0

    while (t+c) % b != 0:
        t += a

    return t // a


def solution(code):
    """
    A brute-force solution would take too much time.

    Write the first equations and start to solve (1) with (2), then (1) with (3), etc
    You can iterate until all you have only on unknown variable.

    Remember that equations such as
    ax+by=1
    when a and b are prime together can be solved with the following solutions:
        x0+b*k
        y0-a*k
    where k is an integer.
    Then multiply solutions by c to solve ax+by=c
    """
    bus_ids = []
    bus_delay = []

    delay = 0
    items = code.split(",")
    for i in range(len(items)):
        if items[i] != "x":
            bus_ids.append(int(items[i]))
            bus_delay.append(delay)
        delay += 1
    print(bus_ids)
    print(bus_delay)

    t_a = bus_ids[0]
    t_b = 0

    for i in range(1, len(bus_ids)):
        c = t_b + bus_delay[i]
        alpha = dioph(t_a, bus_ids[i], c)
        t_b = t_b + t_a*alpha
        t_a = t_a*bus_ids[i]


    i = 0
    while not is_valid(bus_ids, bus_delay, t_a*i+t_b):
        i += 1
    return(t_a*i+t_b)


with open("input.txt", 'r') as f:
    content = f.readlines()
    earliest_depart = int(content[0].strip())
    available_buses = []

    for b in content[1].strip().split(","):
        if b != "x":
            available_buses.append(int(b))

    wait_times = []
    for b in available_buses:
        wait_times.append(b - (earliest_depart % b))

    closest_bus_id = wait_times.index(min(wait_times))
    print(available_buses[closest_bus_id]*wait_times[closest_bus_id])



    assert(solution("7,13,x,x,59,x,31,19") == 1068781)
    print("==")
    assert(solution("17,x,13,19") == 3417)
    print("==")
    assert(solution("67,7,59,61") == 754018)
    print("==")
    assert(solution("67,x,7,59,61") == 779210)
    print("==")
    assert(solution("67,7,x,59,61") == 1261476)
    print("==")
    assert(solution("1789,37,47,1889") == 1202161486)
    print("==")
    print(solution(content[1].strip()))
