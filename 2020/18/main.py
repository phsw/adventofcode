def convert_sentance(s):
    i = 0
    values = []
    while i < len(s):
        if s[i].isdigit():
            n = int(s[i])
            i += 1
            while i < len(s) and s[i].isdigit():
                n *= 10
                n += int(s[i])
                i += 1
            values.append(n)
        elif s[i] == " ":
            i += 1
        elif s[i] == "+":
            values.append("+")
            i += 1
        elif s[i] == "*":
            values.append("*")
            i += 1
        elif s[i] == "(":
            values.append("(")
            i += 1
        elif s[i] == ")":
            values.append(")")
            i += 1

    return values


def compute(values):
    i = 0
    result = None
    op = None
    while i < len(values):
        if values[i] == "(":
            tmp, k = compute(values[i+1:])
            i += k+1
            if op is None:
                result = tmp
            else:
                if op == "+":
                    result += tmp
                elif op == "*":
                    result *= tmp
                op = None
        elif values[i] == ")":
            return result, i
        elif values[i] == "+":
            op = "+"
        elif values[i] == "*":
            op = "*"
        else:
            if op is None:
                result = values[i]
            else:
                if op == "+":
                    result += values[i]
                elif op == "*":
                    result *= values[i]
                op = None
        i += 1

    return result, i


def prod(l):
    p = 1
    for e in l:
        p *= e
    return p


def compute2(values):
    i = 0
    op = None
    numbers = []
    while i < len(values):
        if values[i] == "(":
            tmp, k = compute2(values[i+1:])
            i += k+1
            if op is None:
                numbers.append(tmp)
            else:
                if op == "*":
                    numbers.append(tmp)
                elif op == "+":
                    numbers.append(numbers.pop() + tmp)
                op = None
        elif values[i] == ")":
            return prod(numbers), i
        elif values[i] == "+":
            op = "+"
        elif values[i] == "*":
            op = "*"
        else:
            if op is None:
                numbers.append(values[i])
            else:
                if op == "*":
                    numbers.append(values[i])
                elif op == "+":
                    numbers.append(numbers.pop() + values[i])
                op = None
        i += 1

    return prod(numbers), i


def convert_compute(s):
    values = convert_sentance(s)
    res, _ = compute(values)
    return res


def convert_compute2(s):
    values = convert_sentance(s)
    res, _ = compute2(values)
    return res


assert(convert_compute("1 + 2 * 3 + 4 * 5 + 6") == 71)
assert(convert_compute("1 + (2 * 3) + (4 * (5 + 6))") == 51)
assert(convert_compute("2 * 3 + (4 * 5)") == 26)
assert(convert_compute("5 + (8 * 3 + 9 + 3 * 4 * 3)") == 437)
assert(convert_compute("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") == 12240)
assert(convert_compute("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") == 13632)

assert(convert_compute2("1 + 2 * 3 + 4 * 5 + 6") == 231)
assert(convert_compute2("1 + (2 * 3) + (4 * (5 + 6))") == 51)
assert(convert_compute2("2 * 3 + (4 * 5)") == 46)
assert(convert_compute2("5 + (8 * 3 + 9 + 3 * 4 * 3)") == 1445)
assert(convert_compute2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") == 669060)
assert(convert_compute2("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") == 23340)


with open("input.txt", 'r') as f:
    content = f.readlines()
    s = 0
    s2 = 0
    for l in content:
        s += convert_compute(l.strip())
        s2 += convert_compute2(l.strip())
    print(s)
    print(s2)

