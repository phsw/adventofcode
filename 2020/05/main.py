def convert_code(code):
    row = 0
    for i in range(7):
        if code[6-i] == 'B':
            row += 2**i

    col = 0
    for i in range(3):
        if code[9-i] == 'R':
            col += 2**i

    return row*8 + col


assert(convert_code("FBFBBFFRLR") == 357)
assert(convert_code("BFFFBBFRRR") == 567)
assert(convert_code("FFFBBBFRRR") == 119)
assert(convert_code("BBFFBBFRLL") == 820)


ids = []

with open("input.txt", 'r') as f:
    content = f.readlines()

    for l in content:
        ids.append(convert_code(l))

ids.sort()
print(ids[-1])

for i in range(len(ids)-1):
    if ids[i]+1 != ids[i+1]:
        print(ids[i], ids[i+1], "=>", ids[i]+1)
