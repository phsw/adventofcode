with open("input.txt", 'r') as f:
    content = f.readlines()
    lines = []
    allerg_ingr = dict()
    ingred_allerg = dict()
    list_allerg = []
    list_ingred = []

    for line in content:
        lines.append([[], []])
        in_allerg = False
        for w in line.strip().split():
            if w == "(contains":
                in_allerg = True
            elif in_allerg:
                lines[-1][1].append(w[:-1])
                if w[:-1] not in list_allerg:
                    list_allerg.append(w[:-1])
            else:
                lines[-1][0].append(w)
                if w not in list_ingred:
                    list_ingred.append(w)

    print(list_allerg)
    print(list_ingred)


    while len(allerg_ingr) != len(list_allerg):
        # look for ingredient in all line containing a given allergen:
        for allerg in list_allerg:
            if allerg not in allerg_ingr:
                common_ingred = dict()
                for ingr in list_ingred:
                    if ingr not in ingred_allerg:
                        common_ingred[ingr] = True

                for line in lines:
                    if allerg in line[1]:
                        for ingr in common_ingred:
                            if ingr not in line[0]:
                                common_ingred[ingr] = False

                nb_common = 0
                common = None
                for ingr in common_ingred:
                    if common_ingred[ingr]:
                        common = ingr
                        nb_common += 1

                if nb_common == 1:
                    ingred_allerg[common] = allerg
                    allerg_ingr[allerg] = common




    print(allerg_ingr)


    nb = 0
    for line in lines:
        for e in line[0]:
            if e not in ingred_allerg:
                nb += 1

    print(nb)


    # part 2:
    answer = ""
    for e in sorted(allerg_ingr):
        answer += allerg_ingr[e] + ","
    print(answer[:-1])

