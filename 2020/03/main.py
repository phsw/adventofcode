def get_nb_trees(content, slope_right, slope_down):
    nb_trees = 0
    width = len(content[0].strip())

    y = slope_down
    x = slope_right
    while y < len(content):
        if content[y][x % width] == '#':
            nb_trees += 1

        y += slope_down
        x += slope_right

    return nb_trees

with open("input.txt", 'r') as f:
    content = f.readlines()
    print(get_nb_trees(content, 3, 1))

    to_check = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]
    nb_trees = []
    for slope in to_check:
        nb_trees.append(get_nb_trees(content, *slope))

    p = 1
    for nb in nb_trees:
        p *= nb
    print(nb_trees)
    print(p)


