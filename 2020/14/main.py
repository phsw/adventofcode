with open("input.txt", 'r') as f:
    content = f.readlines()
    mask_value = 0
    add_value = 0
    memory = dict()

    for l in content:
        words = l.strip().split(" = ")

        if words[0] == "mask":
            mask_value = (2**36) - 1  # all 35 bits to 1
            add_value = 0
            for i in range(36):
                if words[1][35-i] != "X":
                    mask_value -= 2**i
                if words[1][35-i] == '1':
                    add_value += 2**i
        else:
            value_to_store = int(words[1])
            address = int(words[0][4:-1])
            memory[address] = (value_to_store & mask_value) + add_value
            # print(value_to_store, "->", memory[address])

    s = 0
    for k in memory:
        s += memory[k]
    print(s)



    # part 2:
    def handle_floattings(floattings, address, value):
        if len(floattings) == 1:
            memory[address | (2**floattings[0])] = value # bit set to 1
            memory[address & (2**36-1-2**floattings[0])] = value # bit set to 0
        else:
            handle_floattings(floattings[1:], address | (2**floattings[0]), value) # bit set to 1
            handle_floattings(floattings[1:], address & (2**36-1-2**floattings[0]), value) # bit set to 1

    floatting = []
    mask = 0
    memory = dict()

    for l in content:
        words = l.strip().split(" = ")

        if words[0] == "mask":
            floatting = []
            mask = 0
            for i in range(36):
                if words[1][35-i] == 'X':
                    floatting.append(i)
                elif words[1][35-i] == '1':
                    mask += 2**i
        else:
            value_to_store = int(words[1])
            address = int(words[0][4:-1])
            address |= mask
            handle_floattings(floatting, address, value_to_store)

    s = 0
    for k in memory:
        s += memory[k]
    print(s)


