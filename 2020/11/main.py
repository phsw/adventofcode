def are_same_grids(g1, g2):
    if len(g1) != len(g2):
        return False
    if len(g1[0]) != len(g2[0]):
        return False

    for i in range(len(g1)):
        for j in range(len(g1[0])):
            if g1[i][j] != g2[i][j]:
                return False

    return True


def get_adjacent_occupied(grid, i, j):
    nb_occupied = 0

    # up
    if i > 0:
        # left
        if j > 0 and grid[i-1][j-1] == "#":
            nb_occupied += 1
        # center
        if grid[i-1][j] == "#":
            nb_occupied += 1
        # right
        if j < len(grid[i-1])-1 and grid[i-1][j+1] == "#":
            nb_occupied += 1

    # left
    if j > 0 and grid[i][j-1] == "#":
        nb_occupied += 1

    # right
    if j < len(grid[i])-1 and grid[i][j+1] == "#":
        nb_occupied += 1

    # down
    if i < len(grid)-1:
        # left
        if j > 0 and grid[i+1][j-1] == "#":
            nb_occupied += 1
        # center
        if grid[i+1][j] == "#":
            nb_occupied += 1
        # right
        if j < len(grid[i-1])-1 and grid[i+1][j+1] == "#":
            nb_occupied += 1

    return nb_occupied


def get_adjacent_occupied2(grid, i, j):
    nb_occupied = 0

    # up, left:
    i2 = i-1
    j2 = j-1
    while i2 >= 0 and j2 >= 0:
        if grid[i2][j2] == ".":
            i2 -= 1
            j2 -= 1
        else:
            if grid[i2][j2] == "#":
                nb_occupied += 1
            break

    # up:
    i2 = i-1
    while i2 >= 0:
        if grid[i2][j] == ".":
            i2 -= 1
        else:
            if grid[i2][j] == "#":
                nb_occupied += 1
            break

    # up, right:
    i2 = i-1
    j2 = j+1
    while i2 >= 0 and j2 < len(grid[i2]):
        if grid[i2][j2] == ".":
            i2 -= 1
            j2 += 1
        else:
            if grid[i2][j2] == "#":
                nb_occupied += 1
            break

    # left:
    j2 = j-1
    while j2 >= 0:
        if grid[i][j2] == ".":
            j2 -= 1
        else:
            if grid[i][j2] == "#":
                nb_occupied += 1
            break

    # right:
    j2 = j+1
    while j2 < len(grid[i]):
        if grid[i][j2] == ".":
            j2 += 1
        else:
            if grid[i][j2] == "#":
                nb_occupied += 1
            break

    # down, left:
    i2 = i+1
    j2 = j-1
    while i2 < len(grid) and j2 >= 0:
        if grid[i2][j2] == ".":
            i2 += 1
            j2 -= 1
        else:
            if grid[i2][j2] == "#":
                nb_occupied += 1
            break

    # down:
    i2 = i+1
    while i2 < len(grid):
        if grid[i2][j] == ".":
            i2 += 1
        else:
            if grid[i2][j] == "#":
                nb_occupied += 1
            break

    # down, right:
    i2 = i+1
    j2 = j+1
    while i2 < len(grid) and j2 < len(grid[i2]):
        if grid[i2][j2] == ".":
            i2 += 1
            j2 += 1
        else:
            if grid[i2][j2] == "#":
                nb_occupied += 1
            break

    return nb_occupied


def get_new_status(grid, i, j):
    if grid[i][j] == ".":
        return "."
    else:
        nb_occupied = get_adjacent_occupied2(grid, i, j)
        if grid[i][j] == "L" and nb_occupied == 0:
            return "#"
        elif grid[i][j] == "#" and nb_occupied >= 5:
            return "L"
        else:
            return grid[i][j]

def display_grid(grid):
    for l in grid:
        print("".join(l))
    print()


def count_occupied_seats(grid):
    nb = 0
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j] == "#":
                nb += 1

    return nb


with open("input.txt", 'r') as f:
    content = f.readlines()
    previous_grid = [[c for c in line.strip()] for line in content]
    new_grid = []
    stabilized = False

    display_grid(previous_grid)

    while not stabilized:
        for i in range(len(previous_grid)):
            new_grid.append([])
            for j in range(len(previous_grid[0])):
                new_grid[i].append(get_new_status(previous_grid, i, j))

        if are_same_grids(previous_grid, new_grid):
            stabilized = True
        else:
            previous_grid = new_grid
            new_grid = []

    display_grid(new_grid)
    print(count_occupied_seats(new_grid))

