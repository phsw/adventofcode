with open("input.txt", 'r') as f:
    content = f.readlines()
    nb_valid = 0

    for line in content:
        items = line.strip().split()
        letter = items[1][0]
        policy = list(map(lambda x: int(x), items[0].split('-')))

        nb_letter = 0
        for c in items[2]:
            if c == letter:
                nb_letter += 1

        if policy[0] <= nb_letter <= policy[1]:
            nb_valid += 1

    print(nb_valid)


    nb_valid = 0

    for line in content:
        items = line.strip().split()
        letter = items[1][0]
        policy = list(map(lambda x: int(x), items[0].split('-')))

        if (items[2][policy[0]-1] == letter) ^ (items[2][policy[1]-1] == letter):
            nb_valid += 1

    print(nb_valid)
