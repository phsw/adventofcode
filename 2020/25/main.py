def transform(subject_number, loop_size):
    v = 1
    for _ in range(loop_size):
        v *= subject_number
        v %= 20201227
    return v

card_subject_number = 7
door_subject_number = 7


def find_loop_size(public_key, subject_number):
    v = 1
    loop_size = 0
    while v != public_key:
        loop_size += 1
        v *= subject_number
        v %= 20201227
    return loop_size


card_public_key = 5764801
card_loop_size = find_loop_size(card_public_key, card_subject_number)
assert(card_loop_size == 8)
door_public_key = 17807724
door_loop_size = find_loop_size(door_public_key, door_subject_number)
assert(door_loop_size == 11)

assert(transform(card_public_key, door_loop_size) == transform(door_public_key, card_loop_size))

def get_encryption_key(public_key_a, public_key_b):
    loop_size_a = find_loop_size(public_key_a, card_subject_number)
    loop_size_b = find_loop_size(public_key_b, door_subject_number)

    return transform(public_key_a, loop_size_b)

assert(get_encryption_key(5764801, 17807724) == 14897079)
print(get_encryption_key(9033205, 9281649))


