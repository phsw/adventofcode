use std::fs;

fn main() {
    let content = fs::read_to_string("input.txt")
        .expect("Something went wrong reading the file");

    let v: Vec<i32> = content.trim().split('\n').map(|s| { s.parse::<i32>().unwrap() }).collect();

    for i in &v {
        for j in &v {
            if i+j == 2020 {
                println!("{}*{} = {}", i, j, i*j);
            }

            for k in &v {
                if i+j+k == 2020 {
                    println!("{}*{}*{} = {}", i, j, k, i*j*k);
                }
            }
        }
    }
}

