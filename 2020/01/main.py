with open("input.txt", 'r') as f:
    content = f.readlines()
    numbers = [int(c) for c in content]

    for i in range(len(numbers)):
        for j in range(i+1, len(numbers)):
            if numbers[i]+numbers[j] == 2020:
                print(numbers[i], numbers[j], numbers[i]*numbers[j])
                break

    for i in range(len(numbers)):
        for j in range(i+1, len(numbers)):
            for k in range(j+1, len(numbers)):
                if numbers[i]+numbers[j]+numbers[k] == 2020:
                    print(numbers[i], numbers[j], numbers[k], numbers[i]*numbers[j]*numbers[k])
                    break
