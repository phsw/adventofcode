def is_sum(target, numbers):
    for i in numbers:
        for j in numbers:
            if i+j == target:
                return True

    return False


with open("input.txt", 'r') as f:
    content = f.readlines()
    numbers = [int(c.strip()) for c in content]
    nb_preemb = 25
    weak = None

    for i in range(nb_preemb, len(numbers)):
        if not is_sum(numbers[i], numbers[i-nb_preemb:i]):
            weak = numbers[i]
            break
    print(weak)

    for i in range(len(numbers)):
        tmp_sum = 0
        j = i
        while tmp_sum != weak and j < len(numbers):
            tmp_sum += numbers[j]
            j += 1
        if tmp_sum == weak:
            print(min(numbers[i:j])+max(numbers[i:j]))
            break




