def count_combinations(numbers):
    if len(numbers) == 1:
        return 1
    else:
        s = count_combinations(numbers[1:])
        if len(numbers) > 2 and (numbers[2] - numbers[0]) <= 3:
            s += count_combinations(numbers[2:])
        if len(numbers) > 3 and (numbers[3] - numbers[0]) <= 3:
            s += count_combinations(numbers[3:])

        return s

def count_combinations2(numbers):
    nb = 1
    number_paths = [0 for _ in numbers]
    number_paths[-1] = 1

    for i in range(len(numbers)-2, -1, -1):
        number_paths[i] = number_paths[i+1]
        if (i+2) < len(numbers) and numbers[i+2]-numbers[i] <= 3:
            number_paths[i] += number_paths[i+2]
        if (i+3) < len(numbers) and numbers[i+3]-numbers[i] <= 3:
            number_paths[i] += number_paths[i+3]

    return number_paths[0]


with open("input.txt", 'r') as f:
    content = f.readlines()
    numbers = [int(c.strip()) for c in content]
    numbers.insert(0, 0)
    numbers.sort()
    print(numbers)
    numbers.append(numbers[-1]+3)
    nb_diff = dict()

    for i in range(1, len(numbers)):
        diff = numbers[i]-numbers[i-1]
        if diff not in nb_diff:
            nb_diff[diff] = 0
        nb_diff[diff] += 1

    print(nb_diff)
    print(nb_diff[1]*nb_diff[3])
    print(count_combinations2(numbers))





