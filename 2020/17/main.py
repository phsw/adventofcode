def count_neighbors(grid, x, y, z):
    nb = 0
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            for k in range(z-1, z+2):
                if i != x or j != y or k != z:
                    if i in grid and j in grid[i] and k in grid[i][j] and grid[i][j][k]:
                        nb += 1

    return nb


def count_active(grid):
    nb = 0

    for i in grid:
        for j in grid[i]:
            for k in grid[i][j]:
                if grid[i][j][k]:
                    nb += 1

    return nb


def print_grid(grid):
    xs = [i for i in grid]
    min_x = min(xs)
    max_x = max(xs)
    ys = []
    for i in grid:
        for j in grid[i]:
            ys.append(j)
    min_y = min(ys)
    max_y = max(ys)
    zs = []
    for i in grid:
        for j in grid[i]:
            for k in grid[i][j]:
                zs.append(j)
    min_z = min(zs)
    max_z = max(zs)

    print("Origin is at ", min_x, min_y)

    for i in range(min_x, max_x+1):
        if i in grid:
            for j in range(min_y, max_y+1):
                if j in grid[i]:
                    if grid[i][j]:
                        print("#", end="")
                    else:
                        print(".", end="")
            print()


with open("input.txt", 'r') as f:
    content = f.readlines()

    grid = dict()

    min_x = 0
    max_x = 0
    min_y = 0
    max_y = 0
    min_z = 0
    max_z = 1

    for i in range(len(content)):  # x
        max_x = max(max_x, i)
        grid[i] = dict()
        line = content[i].strip()
        for j in range(len(line)):  # y
            max_y = max(max_y, j)
            grid[i][j] = dict()
            if line[j] == ".":
                grid[i][j][0] = False
            else:
                grid[i][j][0] = True

    # print_grid(grid)

    for c in range(6):
        # print("\nAfter " + str(c+1) + " cycles:")

        min_x -= 1
        max_x += 1
        min_y -= 1
        max_y += 1
        min_z -= 1
        max_z += 1
        new_grid = dict()
        for i in range(min_x, max_x+1):
            new_grid[i] = dict()
            for j in range(min_y, max_y+1):
                new_grid[i][j] = dict()
                for k in range(min_z, max_z+1):
                    nb = count_neighbors(grid, i, j, k)
                    if i in grid and j in grid[i] and k in grid[i][j] and grid[i][j][k]:
                        new_grid[i][j][k] = (nb in [2, 3])
                    else: # inactive:
                        new_grid[i][j][k] = (nb == 3)
        grid = new_grid
        # print_grid(grid)

    print(count_active(grid))

