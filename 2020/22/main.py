with open("input.txt", 'r') as f:
    content = f.readlines()
    players = []

    for line in content:
        if line.startswith("Player"):
            players.append([])
        elif len(line.strip()) > 0:
            players[-1].append(int(line.strip()))

    print(players)

    r = 0
    while len(players[0]) != 0 and len(players[1]) != 0:
        r += 1
        print("-- Round", r, "--")
        print("Player 1's deck:", players[0])
        print("Player 2's deck:", players[1])

        one_play = players[0].pop(0)
        two_play = players[1].pop(0)

        print("Player 1 plays:", one_play)
        print("Player 2 plays:", two_play)

        assert(one_play != two_play)

        if one_play > two_play:
            print("Player 1 wins the round!")
            players[0].append(one_play)
            players[0].append(two_play)
        else:
            print("Player 2 wins the round!")
            players[1].append(two_play)
            players[1].append(one_play)

        print()

        # if r == 3:
            # break

    print("== Post-game results ==")
    print("Player 1's deck:", players[0])
    print("Player 2's deck:", players[1])

    winner = 0
    if len(players[0]) == 0:
        winner = 1

    score = 0
    for i in range(len(players[winner])):
        score += (i+1)*players[winner][len(players[winner])-i-1]

    print(score)

