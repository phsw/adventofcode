import copy


game_id = 0

def play_game(deck_one, deck_two):
    global game_id
    game_id += 1
    my_game_id = game_id

    players = [deck_one, deck_two]
    rounds = [[], []]
    winner = None
    sub_winner = None
    r = 0
    while len(players[0]) != 0 and len(players[1]) != 0:
        r += 1
        print("-- Round", r, "Game", game_id, "--")
        print("Player 1's deck:", players[0])
        print("Player 2's deck:", players[1])

        if players[0] in rounds[0] and players[1] in rounds[1]:
            winner = 0
            break

        rounds[0].append(copy.deepcopy(players[0]))
        rounds[1].append(copy.deepcopy(players[1]))

        one_play = players[0].pop(0)
        two_play = players[1].pop(0)

        print("Player 1 plays:", one_play)
        print("Player 2 plays:", two_play)

        assert(one_play != two_play)

        if len(players[0]) >= one_play and len(players[1]) >= two_play:
            print("Playing a sub-game to determine the winner...")
            sub_winner, _ = play_game(players[0][:one_play], players[1][:two_play])
            print("...anyway, back to game", my_game_id)
        else:
            if one_play > two_play:
                sub_winner = 0
            else:
                sub_winner = 1

        print("Player", sub_winner+1, "wins the round", r, "of game", my_game_id)
        if sub_winner == 0:
            players[0].append(one_play)
            players[0].append(two_play)
        else:
            players[1].append(two_play)
            players[1].append(one_play)

        print()

    if winner is None:
        winner = 0
        if len(players[0]) == 0:
            winner = 1

    print("The winner of game", my_game_id, "is player", winner+1)

    return winner, players


with open("input.txt", 'r') as f:
    content = f.readlines()
    players = []
    rounds = []

    for line in content:
        if line.startswith("Player"):
            players.append([])
            rounds.append([])
        elif len(line.strip()) > 0:
            players[-1].append(int(line.strip()))

    print(players)

    winner, final_decks = play_game(players[0], players[1])

    print("== Post-game results ==")
    print("Player 1's deck:", final_decks[0])
    print("Player 2's deck:", final_decks[1])

    score = 0
    for i in range(len(final_decks[winner])):
        score += (i+1)*final_decks[winner][len(final_decks[winner])-i-1]

    print(score)

