with open("input.txt", 'r') as f:
    content = f.readlines()

    in_formats = True
    my_ticket = None
    nearby_tickets = []
    i = 0
    allowed_numbers = []
    while i < len(content):
        if in_formats:
            if len(content[i].strip()) == 0:
                in_formats = False
                i += 2
                my_ticket = [int(n) for n in content[i].strip().split(",")]
                i += 2
            else:
                ranges_str = content[i].strip().split(":")[1][1:]
                for r in ranges_str.split(" or "):
                    first = int(r.split("-")[0])
                    last = int(r.split("-")[1])
                    for n in range(first, last+1):
                        if n not in allowed_numbers:
                            allowed_numbers.append(n)
        else:
            nearby_tickets.append([int(n) for n in content[i].strip().split(",")])
        i += 1

    # print(allowed_numbers)
    # print(my_ticket)
    # print(nearby_tickets)

    s = 0
    for ticket in nearby_tickets:
        for n in ticket:
            if n not in allowed_numbers:
                s += n

    print(s)


    # part 2
    in_formats = True
    my_ticket = None
    nearby_tickets = []
    i = 0
    allowed_numbers = []
    rules = dict()
    rules_ids = dict()
    while i < len(content):
        if in_formats:
            if len(content[i].strip()) == 0:
                in_formats = False
                i += 2
                my_ticket = [int(n) for n in content[i].strip().split(",")]
                i += 2
            else:
                ranges_str = content[i].strip().split(":")[1][1:]
                rule_name = content[i].strip().split(":")[0]
                rules[rule_name] = []
                rules_ids[rule_name] = []
                for r in ranges_str.split(" or "):
                    first = int(r.split("-")[0])
                    last = int(r.split("-")[1])
                    rules[rule_name] += list(range(first, last+1))
                    for n in range(first, last+1):
                        if n not in allowed_numbers:
                            allowed_numbers.append(n)
        else:
            nearby_tickets.append([int(n) for n in content[i].strip().split(",")])
        i += 1

    for r in rules_ids:
        rules_ids[r] = [True] * len(rules_ids)

    # print(allowed_numbers)
    # print(my_ticket)
    # print(nearby_tickets)
    # print(rules)

    valid_tickets = [my_ticket]
    for ticket in nearby_tickets:
        keep = True
        for n in ticket:
            if n not in allowed_numbers:
                keep = False
        if keep:
            valid_tickets.append(ticket)

    for ticket in valid_tickets:
        for i in range(len(ticket)):
            for r in rules:
                if ticket[i] not in rules[r]:
                    rules_ids[r][i] = False

    print(rules_ids)

    nb_found = 0
    matches = dict()
    while nb_found < len(rules_ids):
        for r in rules:
            nb_true = 0
            first_true = None
            for i in range(len(rules_ids[r])):
                if rules_ids[r][i]:
                    nb_true += 1
                    first_true = i

            if nb_true == 1 and r not in matches:
                matches[r] = first_true
                for r2 in rules:
                    rules_ids[r2][first_true] = False
                nb_found += 1

    print(matches)

    p = 1
    for r in matches:
        if r.startswith("departure"):
            p *= my_ticket[matches[r]]
    print(p)
