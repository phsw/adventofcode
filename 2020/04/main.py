with open("input.txt", 'r') as f:
    content = f.readlines()
    passes = [{}]

    for i in range(len(content)):
        if len(content[i].strip()) == 0:  # empty line: new passport
            passes.append({})
        else:
            items = content[i].strip().split()
            for item in items:
                splitted = item.split(":")
                key = splitted[0]
                value = splitted[1]
                assert(key not in passes[-1])
                passes[-1][key] = value

    req_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    nb_valid = 0

    for p in passes:
        valid = True
        for f in req_fields:
            if f not in p:
                valid = False

        if valid:
            nb_valid += 1

    print(nb_valid)


    nb_valid = 0

    for p in passes:
        valid = True
        for f in req_fields:
            if f not in p:
                valid = False
            else:
                if f == "byr":
                    if not(len(p[f]) == 4 and 1920 <= int(p[f]) <= 2002):
                        valid = False
                elif f == "iyr":
                    if not(len(p[f]) == 4 and 2010 <= int(p[f]) <= 2020):
                        valid = False
                elif f == "eyr":
                    if not(len(p[f]) == 4 and 2020 <= int(p[f]) <= 2030):
                        valid = False
                elif f == "hgt":
                    if not ((p[f][-2:] == "cm" and 150 <= int(p[f][:-2]) <= 193) or (p[f][-2:] == "in" and 59 <= int(p[f][:-2]) <= 76)):
                        valid = False
                elif f == "hcl":
                    if len(p[f]) == 7 and p[f][0] == "#":
                        for i in range(1, 7):
                            if p[f][i] not in [str(c) for c in range(10)] + ['a', 'b', 'c', 'd', 'e', 'f']:
                                valid = False
                    else:
                        valid = False
                elif f == "ecl":
                    if p[f] not in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]:
                        valid = False
                elif f == "pid":
                    if len(p[f]) == 9:
                        for c in p[f]:
                            if c not in [str(n) for n in range(10)]:
                                valid = False
                    else:
                        valid = False

        print(p, valid)
        if valid:
            nb_valid += 1

    print(nb_valid)
