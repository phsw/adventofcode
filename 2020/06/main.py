with open("input.txt", 'r') as f:
    answers = []
    s = 0
    content = f.readlines()

    for l in content:
        if len(l.strip()) == 0:
            s += len(answers)
            answers = []

        for c in l.strip():
            if c not in answers:
                answers.append(c)

    s += len(answers)
    print(s)


    answers = dict()
    nb_in_group = 0
    s = 0

    for l in content:
        if len(l.strip()) == 0:
            for c in answers:
                if answers[c] == nb_in_group:
                    s += 1

            answers = dict()
            nb_in_group = 0
        else:
            for c in l.strip():
                if c not in answers:
                    answers[c] = 1
                else:
                    answers[c] += 1

            nb_in_group += 1

    for c in answers:
        if answers[c] == nb_in_group:
            s += 1
    print(s)
