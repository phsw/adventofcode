from tqdm import tqdm


NB_TO_PICK = 3
VERBOSE = False


class Cup:
    def __init__(self, label):
        self.label = label
        self.after = self

    def insert_after(self, after_cup):
        after_cup.after = self.after
        self.after = after_cup

    def pop_n_after(self, n):
        start = self.after
        last = start
        labels = []

        for _ in range(n-1):
            labels.append(last.label)
            last = last.after

        labels.append(last.label)
        self.after = last.after
        last.after = start

        return start, last, labels

    def insert_n_after(self, start, last):
        last.after = self.after
        self.after = start


def cups_to_str(start_cup, current_cup):
    s = ""
    c = True
    cup = start_cup

    while c:
        if cup == current_cup:
            s += "(" + str(cup.label) + ")"
        else:
            s += str(cup.label)

        s += " "
        cup = cup.after
        c = (cup != start_cup)

    return s[:-1]


def picked_to_str(start_cup, last_cup):
    s = ""
    cup = start_cup
    c = True
    while c:
        s += str(cup.label) + " "
        c = (cup != last_cup)
        cup = cup.after

    return s[:-1]


def main(s, nb_moves):
    first_cup = None
    last_cup = None
    cups = dict()
    min_label = None
    max_label = 0
    for e in s:
        cup = Cup(int(e))
        cups[cup.label] = cup
        if min_label is None or cup.label < min_label:
            min_label = cup.label
        if cup.label > max_label:
            max_label = cup.label

        if first_cup is None:
            first_cup = cup
            last_cup = first_cup
        else:
            last_cup.insert_after(cup)
            last_cup = cup

    current_cup = first_cup

    for i in range(1000000-len(s)):
        max_label += 1
        cup = Cup(max_label)
        cups[max_label] = cup
        last_cup.insert_after(cup)
        last_cup = cup


    # for i in range(nb_moves):
    for i in tqdm(range(nb_moves), mininterval=2, miniters=30):
        if VERBOSE:
            print("-- move", i+1, "--")
            print("cups:", cups_to_str(first_cup, current_cup))

        first_pick, last_pick, labels = current_cup.pop_n_after(NB_TO_PICK)
        if VERBOSE:
            print("pick up: ", picked_to_str(first_pick, last_pick))

        dest_label = current_cup.label-1
        dest_cup = None

        while dest_label >= min_label and dest_cup is None:
            if dest_label not in labels and dest_label in cups:
                dest_cup = cups[dest_label]
            else:
                dest_label -= 1

        if dest_cup is None:
            dest_label = max_label

            while dest_label in labels:
                dest_label -= 1

            assert(dest_label in cups)
            dest_cup = cups[dest_label]


        if VERBOSE:
            print("destination:", dest_cup.label)

        dest_cup.insert_n_after(first_pick, last_pick)
        current_cup = current_cup.after

        if VERBOSE:
            print()

    if VERBOSE:
        print("-- final --")
        print("cups:", cups_to_str(first_cup, current_cup))

    return cups[1].after.label * cups[1].after.after.label


assert(main("389125467", 10000000) == 149245887792)
print(main("562893147", 10000000))
