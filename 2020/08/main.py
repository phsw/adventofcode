def does_inf_loop(prog):
    i = 0
    acc = 0
    visited_op = []

    while i not in visited_op and i < len(prog):
        visited_op.append(i)
        words = prog[i].strip().split()

        if words[0] == "nop":
            i += 1
        elif words[0] == "acc":
            acc += int(words[1])
            i += 1
        elif words[0] == "jmp":
            i += int(words[1])

    return (i < len(prog)), acc


def change_prog(prog, i):
    new_prog = [l for l in prog]
    words = new_prog[i].strip().split()
    if words[0] == "jmp":
        new_prog[i] = "nop"
    elif words[0] == "nop":
        new_prog[i] = "jmp " + str(words[1])
    else:
        raise Exception("only nop or jump")

    return new_prog


with open("input.txt", 'r') as f:
    content = f.readlines()
    i = 0
    acc = 0
    visited_op = []

    while i not in visited_op:
        visited_op.append(i)
        words = content[i].strip().split()

        if words[0] == "nop":
            i += 1
        elif words[0] == "acc":
            acc += int(words[1])
            i += 1
        elif words[0] == "jmp":
            i += int(words[1])

    print(acc)


    found = False
    i = 0
    while i < len(content) and not found:
        words = content[i].strip().split()

        if words[0] in ["jmp", "nop"]:
            new_prog = change_prog(content, i)
            infinite, acc = does_inf_loop(new_prog)
            if not infinite:
                found = True
                print(acc)
            else:
                i += 1
        else:
            i += 1



