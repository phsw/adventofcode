def find_containers(l, bag):
    if bag in rules:
        for b in rules[bag]:
            if b not in l:
                l.append(b)
                find_containers(l, b)


def count_containers(bag):
    nb = 0
    if bag in rules2:
        for b in rules2[bag]:
            nb += rules2[bag][b] + rules2[bag][b] * count_containers(b)

    return nb


rules = dict()
rules2 = dict()

with open("input.txt", 'r') as f:
    content = f.readlines()

    for l in content:
        words = l.split()
        key_bag_container = " ".join(words[:2])
        assert(key_bag_container not in rules2)
        rules2[key_bag_container] = dict()

        if words[4] != "no":
            i = 5
            while i < len(words):
                key_bag = " ".join(words[i:i+2])
                if key_bag not in rules:
                    rules[key_bag] = []
                rules[key_bag].append(key_bag_container)

                assert(key_bag not in rules2[key_bag_container])
                rules2[key_bag_container][key_bag] = int(words[i-1])

                i += 4

containers = []
find_containers(containers, "shiny gold")
print(containers, len(containers))
print(count_containers("shiny gold"))
