import copy


def validate(s, rules, rule_id):
    verbose = False
    cursor = 0
    threads = [[{
        "rule": rule_id,
        "seq": 0,
        "sub": 0
    }]]

    kept_one = False
    nb_full_rolled = 0
    while cursor < len(s) and len(threads) > 0:
        if verbose:
            print("Must be equal to", s[cursor:])
        added_new_thread = True
        while added_new_thread:
            new_threads = []
            for t in threads:
                # unroll:
                while not isinstance(t[-1], str):
                    last_frame = t[-1]
                    new_rule = rules[last_frame["rule"]][last_frame["seq"]][last_frame["sub"]]
                    if isinstance(rules[new_rule], str):
                        t.append(rules[new_rule])
                    else:
                        for i in range(1, len(rules[new_rule])):
                            new_threads.append(copy.deepcopy(t))
                            new_threads[-1].append({
                                "rule": new_rule,
                                "seq": i,
                                "sub": 0
                            })
                        t.append({
                            "rule": new_rule,
                            "seq": 0,
                            "sub": 0
                        })
            added_new_thread = (len(new_threads) > 0)
            for t in new_threads:
                threads.append(t)
        if verbose:
            print("after unrolling:")
            print(threads)

        kept_threads = []
        for t in threads:
            if t[-1] == s[cursor]:
                kept_threads.append(t)

        kept_one = (len(kept_threads) > 0)

        if verbose:
            print("kept threads:")
            print(kept_threads)

        threads = []
        nb_full_rolled = 0

        for t in kept_threads:
            t.pop()
            t[-1]["sub"] += 1
            while t[-1]["sub"] == len(rules[t[-1]["rule"]][t[-1]["seq"]]):
                t.pop()
                if len(t) > 0:
                    t[-1]["sub"] += 1
                else:
                    break
            if len(t) > 0:
                threads.append(t)
            else:
                nb_full_rolled += 1

        cursor += 1

        if verbose:
            print("after roll:", nb_full_rolled)
            print(threads)
            print("********")

    return (kept_one and cursor == len(s) and nb_full_rolled > 0)


with open("input2.txt", 'r') as f:
    content = f.readlines()

    rules = dict()
    words = []
    in_words = False

    for l in content:
        if in_words:
            words.append(l.strip())
        else:
            if len(l.strip()) == 0:
                in_words = True
            else:
                rule_id = int(l.split(":")[0])

                if '"' in l:
                    rules[rule_id] = l.split('"')[1]
                else:
                    rules[rule_id] = [[]]
                    elements = l.strip().split(":")[1].split(" ")
                    for e in elements:
                        if e == "|":
                            rules[rule_id].append([])
                        elif e.isdigit():
                            rules[rule_id][-1].append(int(e))

    print(rules)
    # print(validate("ab", rules, 0))

    nb = 0
    for w in words:
        res = validate(w, rules, 0)
        if res:
            print(w, "OK")
            nb += 1
        else:
            print(w, "KO")
    print(nb)





